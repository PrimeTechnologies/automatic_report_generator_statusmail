package com.StatusMail.Utility;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonReader 
{
	
	public static List<Map<Object, Object>> readJson(String response) throws JsonParseException, JsonMappingException, IOException
	{
		//JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        Map<Object, Object> map1 = new TreeMap<Object, Object>();
        Map<Object, Object> details_1 = null;
//        
        List<Map<Object, Object>> details_2 = new ArrayList<Map<Object, Object>>();
        ObjectMapper mapper = new ObjectMapper();
        
        try
        {
        	
            //Read JSON file
            Object obj = jsonParser.parse(response);
 
            JSONArray employeeList = (JSONArray) obj;
            
          //Iterating the contents of the array
            Iterator<Object> iterator = employeeList.iterator();
            while(iterator.hasNext())
            {
            	
            		String details = iterator.next().toString();
            		System.out.println(details);
		           //Convert Map to JSON
		            map1 = mapper.readValue(details, new TypeReference<Map<String, Object>>(){});
		            details_1 = new HashMap<Object, Object>();
		         // using for-each loop for iteration over Map.entrySet() 
		            for (Map.Entry<Object,Object> entry : map1.entrySet())
		            {
//		                System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
		            	String key = entry.getKey().toString();
		            	String finalKey = "\""+key+"\"";
		            	String value = entry.getValue().toString();
		            	String finalvalue = "\""+value+"\"";
		            	details_1.put(finalKey,finalvalue);
            	
		            }
		           details_2.add(details_1);
		     } 
           
            System.out.println(details_2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		return details_2;
	}
}
