package com.StatusMail.Utility;

import java.io.IOException;
import java.util.*;  
import javax.mail.*;  
import javax.mail.internet.*;  
import javax.activation.*;  
  
public class SendAttachment{  
 
public static void sendEmail() throws IOException
{  
  
  String to=PropertyReader.propertyFileReader("To");//change accordingly  
  final String user=PropertyReader.propertyFileReader("User");//change accordingly  
  final String password=PropertyReader.propertyFileReader("Password");//change accordingly  
   
  //1) get the session object     
  Properties props = System.getProperties();
  props.setProperty("mail.smtp.auth", "true");
  props.setProperty("mail.smtp.starttls.enable", "true");
  props.setProperty("mail.smtp.host", "smtp.gmail.com");
//  props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//  props.setProperty("mail.smtp.socketFactory.fallback", "false");
  props.setProperty("mail.smtp.port", "587");
//  props.setProperty("mail.smtp.socketFactory.port", "587");
  
//  props.put("mail.smtps.quitwait", "false");

  
  Session session = Session.getDefaultInstance(props,  
   new javax.mail.Authenticator() {  
   protected PasswordAuthentication getPasswordAuthentication() {  
   return new PasswordAuthentication(user,password);  
   }  
  });  
     
  //2) compose message     
  try{  
    MimeMessage message = new MimeMessage(session);  
    message.setFrom(new InternetAddress(user));  
    message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
    message.setSubject("TCOE- Daily Status");  
      
    //3) create MimeBodyPart object and set your message text     
    BodyPart messageBodyPart1 = new MimeBodyPart();  
    messageBodyPart1.setText("<<---- THIS IS AN AUTO-GENERATED MAIL ---->>");
    
    BodyPart messageBodyPart4 = new MimeBodyPart(); 
    messageBodyPart4.setText("<< TCOE VALIDATION DAILY STATUS >>"); 
    
    BodyPart messageBodyPart2 = new MimeBodyPart(); 
    messageBodyPart2.setText("\n"); 
    
    BodyPart messageBodyPart3 = new MimeBodyPart(); 
    messageBodyPart3.setText("\n"); 
    
      
    //4) create new MimeBodyPart object and set DataHandler object to this object      
    MimeBodyPart messageBodyPart5 = new MimeBodyPart();  
  
    String filename = System.getProperty("user.dir") + "\\src\\main\\resources\\TestData\\"+"statusReport.xlsx";  
    DataSource source = new FileDataSource(filename);  
    messageBodyPart5.setDataHandler(new DataHandler(source));  
    messageBodyPart5.setFileName(filename);  
     
     
    //5) create Multipart object and add MimeBodyPart objects to this object      
    Multipart multipart = new MimeMultipart();  
    multipart.addBodyPart(messageBodyPart1);  
    multipart.addBodyPart(messageBodyPart2);  
    multipart.addBodyPart(messageBodyPart3);
    multipart.addBodyPart(messageBodyPart4);
    multipart.addBodyPart(messageBodyPart5);
  
    //6) set the multiplart object to the message object  
    message.setContent(multipart );  
     
    //7) send message  
    Transport.send(message);  
   
   System.out.println("message sent....");  
   }catch (MessagingException ex) {ex.printStackTrace();}  
 }  
}  