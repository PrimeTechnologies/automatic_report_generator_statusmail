package com.StatusMail.Utility;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExportToExcel 
{
	
	public static void JsonToExcelExport(List<Map<Object, Object>> jsondata) throws EncryptedDocumentException, IOException, InterruptedException
	{
		// Get the Local date
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy"); 
		DateTimeFormatter dtfs = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss");
		LocalDateTime now = LocalDateTime.now(); 
		
		String excelFilePath = System.getProperty("user.dir") + "\\src\\main\\resources\\TestData\\"+"statusReport.xlsx";
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		Map<Object, Object> map1 = null;
		deleteFile(excelFilePath);
		
		try
        {
			String FileName = System.getProperty("user.dir") + "\\src\\main\\resources\\TestData\\"+"statusReport.xlsx";
			String FileName_Arch = System.getProperty("user.dir") + "\\src\\main\\resources\\Archive\\"+"statusReport_"+dtfs.format(now)+".xlsx";
			String sheetName = "DailyStatus_"+dtf.format(now);
			// Read the excel file from the defined path
			XSSFWorkbook  workbook = new XSSFWorkbook();
        	XSSFSheet sheet = workbook.createSheet(sheetName); 
        	Cell cell = null;
        	
            
            //Iterating the contents of the array and define the header in excel
            Iterator<Map<Object, Object>> iterator = jsondata.iterator();
            int headerSize = jsondata.iterator().next().size()-1;
            while(iterator.hasNext())
            {
            		// get the json data from json file and read it
            		String details = iterator.next().toString();
            		String finalResponse = details.replace("=", ":");
            		System.out.println(finalResponse);
		           //Convert Map to JSON
		            map1 = mapper.readValue(finalResponse, new TypeReference<Map<Object, Object>>(){});
		            
		         // using for-each loop for iteration over Map.entrySet() 
		            int i =0;
		            Row row = sheet.createRow(1);
		            for (Map.Entry<Object,Object> entry : map1.entrySet())
		            {
		                System.out.println("Key = " + entry.getKey()); 
		                String val = entry.getKey().toString().toUpperCase();
		                cell = row.createCell(i);
		                cell.setCellValue(val);
		                SetBorder(workbook,cell);
		                SetHeaderFont(workbook,cell);
		                i++;            	
		            }
		            break;
		            
            }
            
          //Iterating the contents of the array and define the header in excel
            int j= 2;
            iterator = jsondata.iterator();
            
            while(iterator.hasNext())
            {
            		// get the json data from json file and read it
            		String details = iterator.next().toString();
            		String finalResponse = details.replace("=", ":");
            		System.out.println(finalResponse);
		           //Convert Map to JSON
		            map1 = mapper.readValue(finalResponse, new TypeReference<Map<Object, Object>>(){});
		            
		         // using for-each loop for iteration over Map.entrySet() 
		            int i =0; 
		            Row row = sheet.createRow(j);
		            for (Map.Entry<Object,Object> entry : map1.entrySet())
		            {
		                System.out.println("Value = " + entry.getValue()); 
		                String val = entry.getValue().toString();
		                cell = row.createCell(i);
		                sheet.autoSizeColumn(i);
		                cell.setCellValue(val);
		                SetBorder(workbook,cell);
		                i++;            	
		            }
		            j++;
		            
            }
           
        setTitle(workbook,cell,sheet,sheetName,headerSize);
        FileOutputStream outputStream = new FileOutputStream(FileName);
        FileOutputStream outputStream_Arch = new FileOutputStream(FileName_Arch);
        workbook.write(outputStream);
        workbook.write(outputStream_Arch);
        workbook.close();
        outputStream.close();
        outputStream_Arch.close();
        System.out.println("Successfully  updated the excel and close it");
   
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
	}

	// Set the border for each cell
	public static void SetBorder(Workbook workbook, Cell cell)
	{
		// Style the cell with borders and border color
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        cell.setCellStyle(style);
	}
	
	// Set the font and background color for header
	public static void SetHeaderFont(Workbook workbook, Cell cell)
	{
		CellStyle style = workbook.createCellStyle();
	    style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Font font = workbook.createFont();
            font.setColor(IndexedColors.ROYAL_BLUE.getIndex());
            style.setFont(font);
            cell.setCellStyle(style);
	}
	
	// Delete the existing file in the defined path
	public static void deleteFile(String path) throws InterruptedException
	{
		File file = new File(path); 
        
        if(file.exists()) 
        { 
            file.delete();
        	System.out.println("File deleted successfully"); 
            Thread.sleep(3000);
        } 
        else
        { 
        	System.out.println("Failed to delete the file"); 
        } 
	}
	
	
	public static void setTitle(Workbook workbook, Cell cell,XSSFSheet sheet, String header, int size )
	{
			// Add a row and cell and some text in it.
			Row row = sheet.createRow((short)0);
			cell = row.createCell((short)0);
			cell.setCellValue(header);
		
			// Create a cellRangeAddress to select a range to merge.
			CellRangeAddress cellRangeAddress = new CellRangeAddress(0,0,0,size);
			
			// Merge the selected cells.
			sheet.addMergedRegion(cellRangeAddress);
			CellStyle style = workbook.createCellStyle();
		    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		    style.setVerticalAlignment(VerticalAlignment.CENTER);
		    style.setAlignment(HorizontalAlignment.CENTER);
		    Font font = workbook.createFont();
	            font.setColor(IndexedColors.BLACK.getIndex());
	            style.setFont(font);
	            cell.setCellStyle(style);
	}
	
}
