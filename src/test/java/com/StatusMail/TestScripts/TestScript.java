package com.StatusMail.TestScripts;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.StatusMail.Utility.ExportToExcel;
import com.StatusMail.Utility.JsonReader;
import com.StatusMail.Utility.PropertyReader;
import com.StatusMail.Utility.SendAttachment;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TestScript {

	@Test
    public void getUserDetails() throws ParseException, JsonParseException, JsonMappingException, IOException, EncryptedDocumentException, InterruptedException {
        //The base URI to be used
		String URI = PropertyReader.propertyFileReader("URI");
        RestAssured.baseURI = URI;
        
        //Define the specification of request. Server is specified by baseURI above.
        RequestSpecification httpRequest = RestAssured.given();
 
        //Makes calls to the server using Method type.
        Response response = httpRequest.request(Method.GET, RestAssured.baseURI);
 
        //Checks the Status Code
        int statusCode = response.getStatusCode();
        
        // get the response body
        ResponseBody repBody = response.getBody();
        String finalRespc = repBody.asString();
        Assert.assertEquals(statusCode, 200);
        
        // Read the JSON file
        List<Map<Object, Object>> jsondat = JsonReader.readJson(finalRespc);
        
        // Export the JSON into Excel file
        ExportToExcel.JsonToExcelExport(jsondat);
        
        // Send the status to Respective recipent 
        SendAttachment.sendEmail();
    }
	
	
}
